CREATE Database ApartmentExpensesSystem

USE ApartmentExpensesSystem

CREATE TABLE ApartmentResidents(
	ResidentID INT PRIMARY KEY IDENTITY(1,1),
	FirstName VARCHAR(255) NOT NULL,
	LastName VARCHAR(255) NOT NULL,
	FlatNumber INT NOT NULL,
	OwnerShipType VARCHAR(6) CHECK(OwnerShipType='owner' or OwnerShipType='tenant'),
	ContactNumber BIGINT NOT NULL,
	Email VARCHAR(255),
	MoveInDate DATETIME DEFAULT GETDATE()
)
CREATE PROCEDURE DisplayApartmentResidents
AS
BEGIN
SELECT * FROM ApartmentResidents
END

Exec DisplayApartmentResidents
ALTER PROCEDURE InsertApartmentResidents(@FirstName VARCHAR(255),@LastName VARCHAR(255), @FlatNumber INT, @OwnerShipType Varchar(6),@ContactNumber BIGINT, @Email VARCHAR(255))
AS
BEGIN
INSERT INTO ApartmentResidents (FirstName,LastName,FlatNumber,OwnerShipType,ContactNumber,Email,MoveInDate)
VALUES(@FirstName,@LastName,@FlatNumber,@OwnerShipType,@ContactNumber,@Email,GETDATE())
END
EXEC InsertApartmentResidents 'Tenant1','user',123,'owner',987654,'tenant1@gmail.com'
CREATE PROCEDURE DeleteApartmentResidents(@ResidentId INT)
AS
BEGIN
DELETE FROM ApartmentResidents
WHERE ResidentID=@ResidentId
END
CREATE PROCEDURE EditApartmentResidents(@Id int, @FirstName VARCHAR(255),@LastName VARCHAR(255), @FlatNumber INT, @OwnerShipType Varchar(6),@ContactNumber BIGINT, @Email VARCHAR(255))
AS
BEGIN
UPDATE ApartmentResidents 
SET FirstName=@FirstName,LastName=@LastName,FlatNumber=@FlatNumber,OwnerShipType=@OwnerShipType,ContactNumber=@ContactNumber,Email=@Email
WHERE ResidentID=@Id
END
SELECT * FROM ApartmentResidents
CREATE TABLE  MonthlyMaintenance(
	ID INT PRIMARY KEY IDENTITY(100,1),
	ResidenceID INT References ApartmentResidents(ResidentID),
	[Month] VARCHAR(20),
	[Year] INT,
	MaintenanceAmount decimal(10,2),
	PaymentDate DATETIME,
	PaymentStatus varchar(20) CHECK (PaymentStatus='paid' or PaymentStatus='pending' or PaymentStatus='late'),
	PaymentMethodID INT,
	CategoryId INT,
	Notes VARCHAR(100)
)SELECT * FROM MonthlyMaintenance
Insert into MonthlyMaintenance(ResidenceID,Month,Year,MaintenanceAmount,PaymentDate,PaymentStatus,PaymentMethodID,CategoryId,Notes)
Values(1,'Sep',2022,10500,25-09-2022,'Paid',2,'1',Null)
CREATE TABLE Users(
	UserID INT UNIQUE IDENTITY(1,1),
	UserName varchar(50) PRIMARY KEY,
	[Password] varchar(100),
	Email varchar(100)  UNIQUE,
	FullName varchar(100) NOT NULL,
	CreatedAt DATETIME DEFAULT GETDATE(),
	UpdatedAt DATETIME,
)
Alter PROCEDURE InsertUser(@userName as VARCHAR(50), @password as VARCHAR(100), @email as VARCHAR(100), @fullName as VARCHAR(100))
As
BEGIN
INSERT INTO Users(UserName, [Password],Email,FullName,UpdatedAt) VALUES(@userName, @password, @email, @fullName, GETDATE())
END
EXEC InsertUser 'tenant1','123','tenant1@gmail.com','tenant1 Apartment'
SELECT * FROM Users

CREATE PROCEDURE DisplayUsers
AS
BEGIN
SELECT * FROM USERS
END

CREATE TABLE RoleMaster(
RoleId INT IDENTITY(100,1) PRIMARY KEY,
RoleDescription VARCHAR(255),
CreatedOn DATETIME
)
INSERT INTO RoleMaster(RoleDescription,CreatedOn) VALUES('ower',GETDATE()),('tenant',GETDATE())
SELECT * FROM RoleMaster


SELECT * FROM ApartmentResidents

