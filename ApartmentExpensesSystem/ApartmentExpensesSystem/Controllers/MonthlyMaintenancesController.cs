﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ApartmentExpensesSystem.Models;

namespace ApartmentExpensesSystem.Controllers
{
    public class MonthlyMaintenancesController : Controller
    {
        private MonthlyMaintenanceEntities db = new MonthlyMaintenanceEntities();

        // GET: MonthlyMaintenances
        public ActionResult Index()
        {
            return View(db.MonthlyMaintenances.ToList());
        }

        // GET: MonthlyMaintenances/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonthlyMaintenance monthlyMaintenance = db.MonthlyMaintenances.Find(id);
            if (monthlyMaintenance == null)
            {
                return HttpNotFound();
            }
            return View(monthlyMaintenance);
        }

        // GET: MonthlyMaintenances/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MonthlyMaintenances/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ResidenceID,Month,Year,MaintenanceAmount,PaymentDate,PaymentStatus,PaymentMethodID,CategoryId,Notes")] MonthlyMaintenance monthlyMaintenance)
        {
            if (ModelState.IsValid)
            {
                db.MonthlyMaintenances.Add(monthlyMaintenance);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(monthlyMaintenance);
        }

        // GET: MonthlyMaintenances/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonthlyMaintenance monthlyMaintenance = db.MonthlyMaintenances.Find(id);
            if (monthlyMaintenance == null)
            {
                return HttpNotFound();
            }
            return View(monthlyMaintenance);
        }

        // POST: MonthlyMaintenances/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ResidenceID,Month,Year,MaintenanceAmount,PaymentDate,PaymentStatus,PaymentMethodID,CategoryId,Notes")] MonthlyMaintenance monthlyMaintenance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(monthlyMaintenance).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(monthlyMaintenance);
        }

        // GET: MonthlyMaintenances/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonthlyMaintenance monthlyMaintenance = db.MonthlyMaintenances.Find(id);
            if (monthlyMaintenance == null)
            {
                return HttpNotFound();
            }
            return View(monthlyMaintenance);
        }

        // POST: MonthlyMaintenances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MonthlyMaintenance monthlyMaintenance = db.MonthlyMaintenances.Find(id);
            db.MonthlyMaintenances.Remove(monthlyMaintenance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
