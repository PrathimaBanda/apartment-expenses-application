﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ApartmentExpensesSystem.Models;

namespace ApartmentExpensesSystem.Controllers
{
    public class RegistrationModelsController : Controller
    {
        private RegistrationContext db = new RegistrationContext();

        // GET: RegistrationModels
        public ActionResult Index()
        {
            return View(db.RegistrationModels.ToList());
        }

        // GET: RegistrationModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegistrationModel registrationModel = db.RegistrationModels.Find(id);
            if (registrationModel == null)
            {
                return HttpNotFound();
            }
            return View(registrationModel);
        }

        // GET: RegistrationModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RegistrationModels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserName,Password,Email,FullName")] RegistrationModel registrationModel)
        {
            if (ModelState.IsValid)
            {
                db.RegistrationModels.Add(registrationModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(registrationModel);
        }

        // GET: RegistrationModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegistrationModel registrationModel = db.RegistrationModels.Find(id);
            if (registrationModel == null)
            {
                return HttpNotFound();
            }
            return View(registrationModel);
        }

        // POST: RegistrationModels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,Password,Email,FullName")] RegistrationModel registrationModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registrationModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(registrationModel);
        }

        // GET: RegistrationModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegistrationModel registrationModel = db.RegistrationModels.Find(id);
            if (registrationModel == null)
            {
                return HttpNotFound();
            }
            return View(registrationModel);
        }

        // POST: RegistrationModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RegistrationModel registrationModel = db.RegistrationModels.Find(id);
            db.RegistrationModels.Remove(registrationModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
