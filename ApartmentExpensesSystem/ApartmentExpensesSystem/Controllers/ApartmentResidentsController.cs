﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ApartmentExpensesSystem.Models;

namespace ApartmentExpensesSystem.Controllers
{
    public class ApartmentResidentsController : Controller
    {
        private ApartmentResidentsContext db = new ApartmentResidentsContext();

        // GET: ApartmentResidents
        public ActionResult Index()
        {
            return View(db.ApartmentResidents.ToList());
        }

        // GET: ApartmentResidents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApartmentResidents apartmentResidents = db.ApartmentResidents.Find(id);
            if (apartmentResidents == null)
            {
                return HttpNotFound();
            }
            return View(apartmentResidents);
        }

        // GET: ApartmentResidents/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ResidentId,FirstName,LastName,FlatNumber,OwnerShipType,ContactNumber,Email")] ApartmentResidents apartmentResidents)
        {
            if (ModelState.IsValid)
            {
                db.ApartmentResidents.Add(apartmentResidents);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(apartmentResidents);
        }

        // GET: ApartmentResidents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApartmentResidents apartmentResidents = db.ApartmentResidents.Find(id);
            if (apartmentResidents == null)
            {
                return HttpNotFound();
            }
            return View(apartmentResidents);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ResidentId,FirstName,LastName,FlatNumber,OwnerShipType,ContactNumber,Email")] ApartmentResidents apartmentResidents)
        {
            if (ModelState.IsValid)
            {
                db.Entry(apartmentResidents).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(apartmentResidents);
        }

        // GET: ApartmentResidents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApartmentResidents apartmentResidents = db.ApartmentResidents.Find(id);
            if (apartmentResidents == null)
            {
                return HttpNotFound();
            }
            return View(apartmentResidents);
        }

        // POST: ApartmentResidents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApartmentResidents apartmentResidents = db.ApartmentResidents.Find(id);
            db.ApartmentResidents.Remove(apartmentResidents);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
