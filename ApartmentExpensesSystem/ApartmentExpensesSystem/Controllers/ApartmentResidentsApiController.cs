﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApartmentExpensesSystem.Models;

namespace ApartmentExpensesSystem.Controllers
{
    public class ApartmentResidentsApiController : ApiController
    {
        private ApartmentResidentsContext db = new ApartmentResidentsContext();

        // GET: api/ApartmentResidentsApi
        public IQueryable<ApartmentResidents> GetApartmentResidents()
        {
            return db.ApartmentResidents;
        }

        // GET: api/ApartmentResidentsApi/5
        [ResponseType(typeof(ApartmentResidents))]
        public IHttpActionResult GetApartmentResidents(int id)
        {
            ApartmentResidents apartmentResidents = db.ApartmentResidents.Find(id);
            if (apartmentResidents == null)
            {
                return NotFound();
            }

            return Ok(apartmentResidents);
        }

        // PUT: api/ApartmentResidentsApi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutApartmentResidents(int id, ApartmentResidents apartmentResidents)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apartmentResidents.ResidentId)
            {
                return BadRequest();
            }

            db.Entry(apartmentResidents).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApartmentResidentsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ApartmentResidentsApi
        [ResponseType(typeof(ApartmentResidents))]
        public IHttpActionResult PostApartmentResidents(ApartmentResidents apartmentResidents)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ApartmentResidents.Add(apartmentResidents);
            db.SaveChanges();

            return CreatedAtRoute("", new { id = apartmentResidents.ResidentId }, apartmentResidents);
        }

        // DELETE: api/ApartmentResidentsApi/5
        [ResponseType(typeof(ApartmentResidents))]
        public IHttpActionResult DeleteApartmentResidents(int id)
        {
            ApartmentResidents apartmentResidents = db.ApartmentResidents.Find(id);
            if (apartmentResidents == null)
            {
                return NotFound();
            }

            db.ApartmentResidents.Remove(apartmentResidents);
            db.SaveChanges();

            return Ok(apartmentResidents);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApartmentResidentsExists(int id)
        {
            return db.ApartmentResidents.Count(e => e.ResidentId == id) > 0;
        }
    }
}