﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ApartmentExpensesSystem.Models
{
    public class ApartmentResidentsContext : DbContext
    {
        static string con = ConfigurationManager.ConnectionStrings["Apartment_db"].ToString();
        SqlConnection sqlConnection = new SqlConnection(con);

        public List<ApartmentResidents> Display()
        {
            List<ApartmentResidents> list = new List<ApartmentResidents>();
            try
            {
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand("DisplayApartmentResidents", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    ApartmentResidents user = new ApartmentResidents();
                    user.FirstName = (string)dataReader["FirstName"];
                    user.LastName = (string)dataReader["LastName"];
                    user.FlatNumber = (int)dataReader["FlatNumber"];
                    user.OwnerShipType = (string)dataReader["OwnerShipType"];
                    user.ContactNumber = (long)dataReader["ContactNumber"];
                    user.Email = (string)dataReader["Email"];
                    list.Add(user);
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
                // Handle exception
            }
            finally
            {
                sqlConnection.Close();
            }
            return list;
        }

        public void Insert(ApartmentResidents resident)
        {
            try
            {
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand("InsertApartmentResidents", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FirstName", resident.FirstName);
                cmd.Parameters.AddWithValue("@LastName", resident.LastName);
                cmd.Parameters.AddWithValue("@FlatNumber", resident.FlatNumber);
                cmd.Parameters.AddWithValue("@OwnerShipType", resident.OwnerShipType);
                cmd.Parameters.AddWithValue("@MobileNumber", resident.ContactNumber);
                cmd.Parameters.AddWithValue("@Email", resident.Email);

                int row = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Handle exception
            }
            finally
            {
                sqlConnection.Close();
            }
        }
        public void Delete(int id)
        {
            try
            {
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand("DeleteApartmentResidents", sqlConnection);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.CommandType = CommandType.StoredProcedure;
                int rows = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Handle exception
            }
            finally
            {
                sqlConnection.Close();
            }
        }
        public void Edit(ApartmentResidents resident)
        {
            try
            {
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand("EditApartmentResidents", sqlConnection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", resident.ResidentId);
                cmd.Parameters.AddWithValue("@FirstName", resident.FirstName);

                cmd.Parameters.AddWithValue("@LastName", resident.LastName);

                cmd.Parameters.AddWithValue("@FlatNumber", resident.FlatNumber);

                cmd.Parameters.AddWithValue("@OwnerShipType", resident.OwnerShipType);

                cmd.Parameters.AddWithValue("@ContactNumber", resident.ContactNumber);

                cmd.Parameters.AddWithValue("@Email", resident.Email);

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new Exception("Error occurred while editing resident item.", ex);
            }
            finally
            {
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }

        public System.Data.Entity.DbSet<ApartmentExpensesSystem.Models.ApartmentResidents> ApartmentResidents { get; set; }

    }
}