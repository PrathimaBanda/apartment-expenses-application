﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace ApartmentExpensesSystem.Models
{
    public class LoginContext
    {
        public bool findUser(string username, string password)
        {
            RegistrationContext registrationContext = new RegistrationContext();

            List<RegistrationModel> registeredUsersDetails = registrationContext.Display();
            var result = registeredUsersDetails.FirstOrDefault(x => x.UserName == username && x.Password == password);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }

}