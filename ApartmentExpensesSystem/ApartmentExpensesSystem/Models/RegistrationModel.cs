﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApartmentExpensesSystem.Models
{
    public class RegistrationModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter your Name")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please enter your Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter your Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter your Full Name")]
        public string FullName { get; set; }

    }
}