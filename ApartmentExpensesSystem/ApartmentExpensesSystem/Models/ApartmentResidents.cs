﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApartmentExpensesSystem.Models
{
    public class ApartmentResidents
    {
        [Key]
        public int ResidentId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public int FlatNumber { get; set; }
        [Required]
        public string OwnerShipType { get; set; }
        [Required]
        public long ContactNumber { get; set; }
        [Required]
        public string Email { get; set; }
    }
}