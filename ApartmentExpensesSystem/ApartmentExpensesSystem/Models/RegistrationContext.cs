﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApartmentExpensesSystem.Models
{
    public class RegistrationContext : DbContext
    {
        static string con = ConfigurationManager.ConnectionStrings["Apartment_db"].ToString();
        SqlConnection sqlConnection = new SqlConnection(con);

        public List<RegistrationModel> Display()
        {
            List<RegistrationModel> list = new List<RegistrationModel>();
            try
            {
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand("DisplayUsers", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    RegistrationModel user = new RegistrationModel();
                    user.UserName = (string)dataReader["userName"];
                    user.Password = (string)dataReader["password"];
                    user.Email = (string)dataReader["email"];
                    user.FullName = (string)dataReader["fullName"];
                    list.Add(user);
                }
                dataReader.Close();
            }
            catch (Exception ex)
            {
                // Handle exception
            }
            finally
            {
                sqlConnection.Close();
            }
            return list;
        }

        public void Insert(RegistrationModel user)
        {
            try
            {
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand("InsertUser", sqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userName", user.UserName);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@email", user.Email);
                cmd.Parameters.AddWithValue("@fullName", user.FullName);

                int row = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Handle exception
            }
            finally
            {
                sqlConnection.Close();
            }
        }
        public System.Data.Entity.DbSet<ApartmentExpensesSystem.Models.RegistrationModel> RegistrationModels { get; set; }

        public System.Data.Entity.DbSet<ApartmentExpensesSystem.Models.LoginModel> LoginModels { get; set; }

        public System.Data.Entity.DbSet<ApartmentExpensesSystem.Models.ApartmentResidents> ApartmentResidents { get; set; }
    }
}